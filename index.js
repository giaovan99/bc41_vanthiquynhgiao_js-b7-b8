var arr=[];
var subArr=[];
var bt9Arr=[];

// Thêm phần tử vào mảng gốc và mảng phụ
document.querySelector('button').addEventListener('click', function(){
    // Thêm phần tử vào mảng arr
    var n=document.querySelector('input').value*1;
    arr.push(n); /** Thêm phần tử vào mảng chính arr */
    subArr.push(n); /** Thêm phần tử vào mảng phụ subArr */
    document.querySelector('p').innerHTML=`<b>${arr}</b> `
});

// Sắp xếp mảng từ nhỏ -> lớn
function sortArray(arr){
    var sortArr = arr.map(function(n) {
        return n;
    }); 
    sortArr.sort(function(a, b){return a - b});
    return sortArr;
};

// Tạo ra mảng số dương
function positiveArr(arr){
    return arr.filter(function(n){
        return n>0;
    })
};

// Tạo ra mảng số âm
function negativeArr(arr){
    return arr.filter(function(n){
        return n<0;
    })
};

// Kiểm tra só nguyên tố
function primeNumber(n){
    if (n<2){
        return false;
    }else if (n==2 || n==3){
        return true;
    }else{ 
        for (var i=2; i<=Math.sqrt(n); i++){
            if (n%i==0){
                return false
            }
        }
    }
    return true;
}

// start BT1
document.querySelector('#totalIntergen').addEventListener('click',function(){
    var sum=0;
    for (var i=0; i<positiveArr(arr).length; i++){
        sum=sum+positiveArr(arr)[i];
    }
    document.getElementById('totalIntergenResult').innerHTML=`${sum}`;
});
// end BT1

// start BT2
document.querySelector('#countPositiveNumber').addEventListener('click',function(){
    var count=0;
    console.log(positiveArr(arr).length);
    document.getElementById('countPositiveNumberResult').innerHTML= positiveArr(arr).length;
});
// end BT2

// start BT3
document.querySelector('#findMinNumber').addEventListener('click',function(){
    document.getElementById('findMinNumberrResult').innerHTML= sortArray(arr)[0];
});
// end BT3

// start BT4
document.querySelector('#findMinPositiveNumber').addEventListener('click',function(){
    document.getElementById('findMinPositiveNumberResult').innerHTML= sortArray(positiveArr(arr))[0];
});
// end BT4

// start BT5
document.querySelector('#findOddLastest').addEventListener('click',function(){
    var lastOdd=-1;
    for (var i=0; i<arr.length; i++){
        if(arr[i]%2==0){
            lastOdd=arr[i];
        }
    }
    document.getElementById('findOddLastestResult').innerHTML= lastOdd;
});
// end BT5

// start BT6
document.querySelector('#tradePosition').addEventListener('click',function(){
    var index1=document.getElementById('index1').value*1;
    var index2=document.getElementById('index2').value*1;
    var num1 = subArr[index1];
    var num2 = subArr[index2];
    subArr[index1]=num2;
    subArr[index2]=num1;
    document.getElementById('tradePositionResult').innerHTML=`Vị trí sau hoán đổi: ${subArr} `
});
// end BT6

// start BT7
document.querySelector('#sortArray2').addEventListener('click',function(){
    document.getElementById('sortArray2Result').innerHTML=` Vị trí sau sắp xếp: ${sortArray(arr)}`;
});
// end BT7

// start BT8
document.querySelector('#firstPrimeNumber').addEventListener('click',function(){
    var result=-1; /**Mặc định mảng không có số nguyên tố */
    for (var i=0; i<arr.length; i++){
        if (primeNumber(arr[i])){
           result=arr[i]; /**Ghi nhận số nguyên tố */
        }
        break;        /**Rời khỏi vòng lặp */
    }
    document.getElementById('firstPrimeNumberResult').innerHTML=`${result}`;
});
// end BT8

// start BT9
// Thêm mảng vào bt9Arr
document.querySelector('#addNumberBT9').addEventListener('click',function(){
    var n=document.getElementById('inputbt9').value*1;
    bt9Arr.push(n); /** Thêm phần tử vào mảng bt9Arr */
    document.getElementById('pBT9').innerHTML=`<b>${bt9Arr}</b> `
});

// Xử lý mảng: đếm số nguyên
document.querySelector('#countInteger').addEventListener('click',function(){
    var count=0;
    for (var i=0; i<bt9Arr.length; i++){
        if (Number.isInteger(bt9Arr[i])){
            count++;
        }
    }
    document.getElementById('countIntegerResult').innerHTML=` Số nguyên: ${count}`;
});
// end BT9

// start BT10
document.querySelector('#compareQuantity').addEventListener('click',function(){
    var result='';
    if (positiveArr(arr).length==negativeArr(arr).length){
        result='Số dương = Số âm';
    } else if (positiveArr(arr).length>negativeArr(arr).length){
        result='Số dương > Số âm';
    } else {
        result='Số dương < Số âm';
    }
    document.getElementById('compareQuantityResult').innerHTML=result;
});
// end BT10

